package br.com.geosales;

import static br.com.geosales.components.GSToast.toastAviso;
import static br.com.geosales.components.GSToast.toastFalha;
import static br.com.geosales.components.GSToast.toastSucesso;

import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

import br.com.geosales.components.GSColorPicker;
import gwt.material.design.client.ui.MaterialListBox;

public class App implements EntryPoint {

	public List<String> listaComponentes = java.util.Arrays.asList("GSColorPicker", "GSToast", "GSComboBox");
	
	public void onModuleLoad() {
		java.util.Collections.sort(listaComponentes);
		
		MaterialListBox mlb = new MaterialListBox();
		RootPanel.get("sendButtonContainer").add(mlb);
		listaComponentes.forEach(elemento -> mlb.add(elemento));
		
		final Button callComponent = new Button("Chamar componente");
		RootPanel.get("sendButtonContainer").add(callComponent);

		callComponent.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (RootPanel.get("componente").getWidgetCount() == 0) {
					chamarComponente(mlb.getSelectedValue());
					callComponent.setText("Eliminar componente");
				} else {
					RootPanel.get("componente").clear();
					callComponent.setText("Chamar componente");
				}
			}
		});
	}
	public void chamarComponente(String nome) {
		switch (nome) {
		case "GSColorPicker":
			RootPanel.get("componente").add(new GSColorPicker());
			break;
			
		case "GSToast":
			final TextBox textoToast = new TextBox();
			textoToast.setText("Texto de teste");
			final Button sucesso = new Button("Sucesso");
			final Button aviso = new Button("Aviso");
			final Button falha = new Button("Falha");
			RootPanel.get("componente").add(textoToast);
			RootPanel.get("componente").add(sucesso);
			RootPanel.get("componente").add(aviso);
			RootPanel.get("componente").add(falha);

			sucesso.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					toastSucesso(textoToast.getText());
				}
			});
			aviso.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					toastAviso(textoToast.getText());
				}
			});
			falha.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					toastFalha(textoToast.getText());
				}
			});
			break;
			
		case "GSComboBox":
			GSComboBoxSample sample = new GSComboBoxSample();
			RootPanel.get("componente").add(sample);
			break;
			
		default:
			break;
		}
	}
}
