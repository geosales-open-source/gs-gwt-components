package br.com.geosales;

import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import br.com.geosales.components.*;


public class GSComboBoxSample extends Composite {

	interface GSComboBoxSampleUiBinder extends UiBinder<Widget, GSComboBoxSample> {
	}

	private static final GSComboBoxSampleUiBinder uiBinder = GWT.create(GSComboBoxSampleUiBinder.class);

	@UiField GSComboBox<String> GSCBSample;

	public GSComboBoxSample() {
		initWidget(uiBinder.createAndBindUi(this));
	}
}
