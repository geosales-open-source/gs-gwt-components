<details><summary>TLDR; - Só quero rodar o portalzinho</summary>

Abra 2 consoles e em cada console coloque:

`./mvnw gwt:codeserver -pl *-client -am`

`./mvnw jetty:run -pl *-server -am -Denv=dev`
</details>

# GS-GWT-COMPONENTS-PORTAL-TEST

Esse é o nome completo do que foi apelidado pelo Jeff de portalzinho. Esse projeto consiste basicamente em um projeto gwt para testarmos isoladamente os componentes do nuPortal, semelhante ao que vemos em libs de componentes como MaterialDesign. No momento que estou escrevendo esse mini-guia ainda não possuímos uma forma de deixar o projeto sempre online para realizar os testes, logo precisamos subir o portalzinho localmente.

Se seu desejo é só subir o portalzinho local para testar os componentes já adicionados, com dois terminais abertos na pasta do portalzinho rode ambos comandos.

`./mvnw gwt:codeserver -pl *-client -am`

`./mvnw jetty:run -pl *-server -am -Denv=dev`

Para finalizar basta com CTRL+C, porém há chances de um dos terminais continuar rodando em modo zumbi, então caso não consiga rodar uma segunda vez, é bom verificar no gerenciador de tarefas.

Agora, se você deseja adicionar um componente para testes no portalzinho, mais uma vez usando o GSColorPicker como cobaia:

1 - Adicione o nome que ele será chamado na lista de strings listaComponentes (L27)

2 - No método `chamarComponente()` adicione o `case` da string adicionada

3 - Como a intenção é apenas mostrar o componente, basta usar o método `RootPanel.get("componente").add(___);` onde o ___ será a classe do componente desejado de acordo com o uso. No caso do ColorPicker usei um `new GSColorPicker()`

3.1 - Aproveitando para citar a exceção do caso, o componente GSComboBox não possuia um ui.xml quando enviamos o componente para a lib. Nesse caso, criei uma classe chamada GSComboBoxSample e o respectivo ui.xml, com isso foi possível exibir a GSComboBox no portalzinho sem maiores problemas.

4 - Com isso, basta iniciar o portalzinho da forma que foi explicada mais em cima.
