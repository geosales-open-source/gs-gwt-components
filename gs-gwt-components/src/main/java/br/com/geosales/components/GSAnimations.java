package br.com.geosales.components;

import gwt.material.design.client.ui.animate.Transition;

public class GSAnimations {

	public static final Transition CARREGAMENTO_INTERNO = Transition.PULSE;
	public static final Transition CARREGAMENTO_MENU = Transition.SLIDEINLEFT;
	public static final Transition CARREGAMENTO_BUSCA = Transition.SLIDEINDOWN;
}
