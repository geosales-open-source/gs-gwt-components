package br.com.geosales.components.resources.messages;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;
import com.google.gwt.i18n.server.testing.Gender;

@Generate(format = "com.google.gwt.i18n.server.PropertyCatalogFactory")
@LocalizableResource.DefaultLocale
public interface GSMessage extends Messages {

	@DefaultMessage("Nenhum registro encontrado")
	String nenhumRegistro();

	public static final class Util { 
		private static GSMessage instance;

		public static final GSMessage getInstance() {
			if (instance == null) {
				instance = GWT.create(GSMessage.class);
			}
			return instance;
		}

		private Util() {
			// Utility class should not be instantiated
		}
	}
}
