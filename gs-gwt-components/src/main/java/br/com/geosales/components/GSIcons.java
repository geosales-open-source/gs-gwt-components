package br.com.geosales.components;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import gwt.material.design.client.constants.IconSize;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.ui.MaterialIcon;
import gwt.material.design.client.ui.MaterialPanel;

public class GSIcons extends Composite implements GSComponent {
	interface GSIconsUiBinder extends UiBinder<Widget, GSIcons> {
	}

	private static GSIconsUiBinder uiBinder = GWT.create(GSIconsUiBinder.class);

	@UiField MaterialPanel panel;

	public GSIcons(String tipoIcone, String nomeIcone, String iconSize) {
		initWidget(uiBinder.createAndBindUi(this));
		gerarIcone(tipoIcone, nomeIcone, iconSize);
	}

	public GSIcons(String tipoIcone, String nomeIcone) {
		initWidget(uiBinder.createAndBindUi(this));
		gerarIcone(tipoIcone, nomeIcone, "small");
	}

	private void gerarIcone(String tipoIcone, String nomeIcone, String iconSize) {

		MaterialIcon widgets = new MaterialIcon();
		switch(tipoIcone) {
			case "mt":
				widgets.setVisible(true);
				widgets.setIconSize(IconSize.fromStyleName(iconSize));
				widgets.setIconType(IconType.fromStyleName(nomeIcone));
				panel.add(widgets);
				break;

			default:
				widgets.setVisible(true);
				widgets.setIconSize(IconSize.SMALL);
				widgets.setIconType(IconType.CLOSE);
				panel.add(widgets);
		}

		if(tipoIcone.equalsIgnoreCase("mt")){

		}
	}

	public void setVisible(boolean visible) {
		panel.setVisible(visible);
	}
}
