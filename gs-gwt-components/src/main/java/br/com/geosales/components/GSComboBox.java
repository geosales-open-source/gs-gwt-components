package br.com.geosales.components;

import static gwt.material.design.addins.client.combobox.js.JsComboBox.$;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.Timer;

import br.com.geosales.components.resources.messages.GSMessage;
import br.com.softsite.toolbox.Toolbox;
import gwt.material.design.addins.client.combobox.MaterialComboBox;
import gwt.material.design.addins.client.combobox.js.LanguageOptions;

public class GSComboBox<T> extends MaterialComboBox<T> {

	private static final int VAL_TIME_CALLBACK_BUSCA = 400;
	private boolean allowBlank;
	private boolean allowCallback;
	private Runnable callback;
	private Function<T, String> presenter;
	private GSMessage message = GSMessage.Util.getInstance();

	private Timer timeCallbackBusca = new Timer() {
		@Override
		public void run() {
			Toolbox.runIfNotNull(callback, Runnable::run);
		}
	};

	private void carregarListaCombobox(String textoFiltro) {
		$(getElement()).find(".select2-search__field").click();
		$(getElement()).find(".select2-search__field").val(textoFiltro);
		$(getElement()).find(".select2-search__field").click();
	}

	public String getTextoEscolha() {
		return $(getElement()).find(".select2-search__field").val().toString();
	}

	private void agendarPesquisa() {
		timeCallbackBusca.cancel();
		timeCallbackBusca.schedule(VAL_TIME_CALLBACK_BUSCA);
	}

	@UiConstructor
	public GSComboBox() {
		this(Object::toString);
	}

	public GSComboBox(Function<T, String> presenter) {
		super();

		setLanguage(getDefaultLanguage());
		setPresenter(presenter);
	}

	protected LanguageOptions getDefaultLanguage() {
		LanguageOptions languageOptions = new LanguageOptions();
		languageOptions.setNoResults(param1 -> message.nenhumRegistro());
		return languageOptions;
	}

	public void setPresenter(Function<T, String> presenter) {
		Objects.requireNonNull(presenter);
		this.presenter = presenter;
	}

	@Override
    protected void onLoad() {
		super.onLoad();

		setKeyFactory(t -> Toolbox.getIfNotNull(t, presenter, ""));

		if (isAllowCallback()) {
			registerHandler(addKeyUpHandler(key -> agendarPesquisa()));
		}
	}

	public void setAllowBlank(boolean allowBlank) {
    	this.allowBlank = allowBlank;
    }

	public boolean isAllowBlank() {
    	return this.allowBlank;
    }

	public void setAllowCallback(boolean allowCallback) {
    	this.allowCallback = allowCallback;
    }

	public boolean isAllowCallback() {
    	return this.allowCallback;
    }

	public void handleCallback(List<T> result) {
		List<T> values = getSelectedValues();

		setAcceptableValues(Stream.concat(values.stream(), result.stream()).collect(Collectors.toList()));
		setValues(values);
		carregarListaCombobox(getTextoEscolha());
	}

	public void setCallback(Runnable callback) {
		this.callback = callback;
	}

	@Override
	public List<T> getSelectedValues() {
		return super.getSelectedValues().stream().filter(Objects::nonNull).collect(Collectors.toList());
	}

	@Override
	public void setAcceptableValues(Collection<T> values) {
    	clear();
    	if (isAllowBlank()) {
    		addItem(null);
    	}
    	values.forEach(this::addItem);
    }
}