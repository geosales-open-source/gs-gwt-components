package br.com.geosales.components;

import gwt.material.design.client.ui.MaterialToast;

public class GSToast extends MaterialToast {

	public enum TipoToast {
		SUCESSO, FALHA, AVISO
	}

	private String mensagem;
	private int tempo;
	private TipoToast tipo;

	private static final int tempoDefaultToast = 3000;

	private static String CSSToastSucesso = "green";
	private static String CSSToastFalha = "red";

	public GSToast(String mensagem, int tempo, TipoToast tipo) {
		this.mensagem = mensagem;
		this.tempo = tempo;
		this.tipo = tipo;
	}

	public static void toastSucesso(String mensagem, int tempo) {
		MaterialToast.fireToast(mensagem, tempo, CSSToastSucesso);
	}

	public static void toastSucesso(String mensagem) {
		MaterialToast.fireToast(mensagem, tempoDefaultToast, CSSToastSucesso);
	}

	public static void toastFalha(String mensagem, int tempo) {
		MaterialToast.fireToast(mensagem, tempo, CSSToastFalha);
	}

	public static void toastFalha(String mensagem) {
		MaterialToast.fireToast(mensagem, tempoDefaultToast, CSSToastFalha);
	}

	public static void toastAviso(String mensagem, int tempo) {
		MaterialToast.fireToast(mensagem, tempo);
	}

	public static void toastAviso(String mensagem) {
		MaterialToast.fireToast(mensagem, tempoDefaultToast);
	}

	public void disparaToast() {
		switch (tipo) {
		case SUCESSO:
			toastSucesso(mensagem, tempo);
			break;
		case FALHA:
			toastFalha(mensagem, tempo);
			break;
		case AVISO:
			toastAviso(mensagem, tempo);
			break;
		}
	}

	public String getMensagem() {
		return mensagem;
	}

	public int getTempo() {
		return tempo;
	}

	public TipoToast getTipo() {
		return tipo;
	}
}
