package br.com.geosales.components;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialPanel;

public class GSColorPicker extends Composite implements GSComponent {
	interface GSColorPickerUiBinder extends UiBinder<Widget, GSColorPicker> {
	}

	private static GSColorPickerUiBinder uiBinder = GWT.create(GSColorPickerUiBinder.class);

	@UiField MaterialPanel panel, colorSelected;
	@UiField MaterialButton pickedColor;
	@UiField GSColorPallete colorPallete;

	String colorClass;

	public GSColorPicker() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	protected void onLoad() {
		super.onLoad();

		colorPallete.setColorPicker(this);
		pickedColor.addClickHandler(c -> {
			colorPallete.removeStyleName("esconder");
			pickedColor.addStyleName("btn--open");
		});
	}

	public void setPickerColor(String colorClass) {
		this.colorClass = colorClass;
		colorSelected.setStyleName("color-selected " + colorClass);
	}

	public String getPickedColor() {
		return colorClass;
	}

	public String getPickedHexaColor() {
		switch (colorClass){
			case("gs-color-blue"):
				return "4848AF";
			case("gs-color-graphite"):
				return "484848";
			case("gs-color-green"):
				return "5C965C";
			case("gs-color-maroon"):
				return "9B4848";
			case("gs-color-orange"):
				return "FF7A00";
			case("gs-color-pink"):
				return "DC7070";
			case("gs-color-red"):
				return "F53434";
			case("gs-color-yellow"):
				return "E1AC3E";
			case("gs-color-blue2"):
				return "4646B4";
			case("gs-color-red2"):
				return "C8322D";
			case("gs-color-steel-blue"):
				return "416ba9";
			case("gs-color-light-blue"):
				return "7683c1";
			case("gs-color-light-turquoise"):
				return "62cbc9";
			case("gs-color-blue-gray"):
				return "c4cada";
			case("gs-color-dark-gray"):
				return "58595B";
			default:
				return "5C965C";
		}
	}

	public String getCssColorPicked(String colorHex) {
		switch (colorHex) {
			case "#4848AF":
				return "gs-color-blue";
			case "#484848":
				return "gs-color-graphite";
			case "#5C965C":
				return "gs-color-green";
			case "#9B4848":
				return "gs-color-maroon";
			case "#FF7A00":
				return "gs-color-orange";
			case "#DC7070":
				return "gs-color-pink";
			case "#F53434":
				return "gs-color-red";
			case "#E1AC3E":
				return "gs-color-yellow";
			case "#4646B4":
				return "gs-color-blue2";
			case "#C8322D":
				return "gs-color-red2";
			case "#416ba9":
				return "gs-color-steel-blue";
			case "#7683c1":
				return "gs-color-light-blue";
			case "#62cbc9":
				return "gs-color-light-turquoise";
			case "#c4cada":
				return "gs-color-blue-gray";
			case "#58595B":
				return "gs-color-dark-gray";
			default:
				return "#5C965C";
		}
	}
}