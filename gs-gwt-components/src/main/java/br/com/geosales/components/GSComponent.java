package br.com.geosales.components;

import com.google.gwt.user.client.ui.Composite;

public interface GSComponent {

	default Composite asComposite() {
		return (Composite) this;
	}

	default void prepareDebug() {}
}
