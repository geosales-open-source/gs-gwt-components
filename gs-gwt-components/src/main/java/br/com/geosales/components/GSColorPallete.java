package br.com.geosales.components;

import java.util.stream.Stream;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import gwt.material.design.client.ui.MaterialPanel;

class GSColorPallete extends Composite implements GSComponent {
	interface GSColorPalleteUiBinder extends UiBinder<Widget, GSColorPallete> {
	}

	private static GSColorPalleteUiBinder uiBinder = GWT.create(GSColorPalleteUiBinder.class);

	@UiField MaterialPanel cor1, cor2, cor3, cor4, cor5, cor6, cor7, cor8, cor9, cor10, cor11, cor12, cor13, cor14, cor15;

	private GSColorPicker colorPicker;

	GSColorPallete() {
		initWidget(uiBinder.createAndBindUi(this));

		Stream.of(cor1, cor2, cor3, cor4, cor5, cor6, cor7, cor8, cor9, cor10, cor11, cor12, cor13, cor14, cor15).forEach(c -> c.addClickHandler(h -> {
			this.colorPicker.pickedColor.removeStyleName("btn--open");
			this.colorPicker.colorPallete.addStyleName("esconder");
			setChoosedColor(c.getStyleName());
		}));
	}

	private void setChoosedColor(String colorClass) {
		this.colorPicker.colorClass = colorClass.split(" ")[1];
		this.colorPicker.colorSelected.setStyleName(colorClass);
	}

	void setColorPicker(GSColorPicker colorPicker) {
		this.colorPicker = colorPicker;
	}
}