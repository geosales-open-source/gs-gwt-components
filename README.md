# GS-GWT-LIB

Agora que temos uma lib para armazenar os nossos componentes GWT do nuPortal, precisamos saber como propriamente importá-los e substituir o componente do nuPortal pelo da lib. Esse readme na verdade é a parte 1/3 do pequeno guia de uso da lib + portalzinho e substituição dos componentes no nuPortal.

## Extração

A extração dos costuma ser bem simples, vou usar de exemplo o componente GSColorPicker que aparenta ser um componente de dificuldade intermediária.

1 - Iniciamos copiando a classe java e seu respectivo ui.xml para a pasta components Nesse caso em específico, o componente utiliza também componente GSColorPallete, então importamos o outro componente e o ui.xml.

2 - Nesse exemplo, o caminho da lib deve ser alterado no GSColorPicker.ui.xml. A maioria dos componentes que importei até o momento não costumam precisar desse passo, mas pode ser que o componente que você deseja importar seja um componente de componentes.

3 - Agora vamos importar o CSS, atualmente a pasta que eles estão sendo armazenados é a public/css, com excessão de ter certeza que você está copiando o arquivo certo, esse passo não deve ter nenhuma particularidade.

4 - Para finalizar a importação, devemos dizer ao módulo onde está o css que desejamos carregar, então adicionamos as seguintes linhas ao module.gwt.xml
```
<stylesheet src="css/gs-color-pallete.css"/>
<stylesheet src="css/gs-color-picker.css"/>
```

5 - Por fim, verificamos se os pacotes estão corretamente escritos e `./mvnw install` nessa lib.

## Rodando portalzinho

Rode o codeserver para o `:portal-client`:

```bash
./mvnw -pl :portal-client -am gwt:codeserver
```

Então rode o Jetty para o `:portal-server`:

```bash
./mvnw -pl :portal-server -am jetty:run -Denv=dev
```

_Voi là!_ Acesse http://localhost:8080 e seja feliz agora!
